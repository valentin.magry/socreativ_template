FROM wordpress:latest

# SSMTP instal & configuration
RUN curl --location --output /usr/local/bin/mhsendmail https://github.com/mailhog/mhsendmail/releases/download/v0.2.0/mhsendmail_linux_amd64 && \
    chmod +x /usr/local/bin/mhsendmail
RUN echo 'sendmail_path="/usr/local/bin/mhsendmail --smtp-addr=mailhog:1025 --from=dev@socreativ.docker"' > /usr/local/etc/php/conf.d/mailhog.ini

# Add usual plugins
ADD plugins /var/www/html/wp-content/plugins/
# Remove default plugins
# RUN rm /var/www/html/wp-content/plugins/hello.php && rm -r /var/www/html/wp-content/plugins/akismet

# Add custom php.ini
COPY custom.ini $PHP_INI_DIR/conf.d/


EXPOSE 80